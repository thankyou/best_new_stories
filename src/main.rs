use chrono::prelude::*;
use chrono::Duration;
use serde::{Deserialize, Serialize};
use std::collections::hash_map::RandomState;
use std::collections::HashSet;
use std::fs::{create_dir, File};
use std::hash::Hash;
use std::io::Write;

const N_DAILY_STORIES_OUTPUT: usize = 10;
const N_DAYS: i64 = 20;
const N_PAGES: u32 = 25;
const OLDEST_DATE_HN: i64 = 4;

#[derive(Deserialize, Debug)]
struct RequestResult {
    hits: Option<Vec<StoryResult>>,
}

#[derive(Deserialize, Debug, Eq, Hash)]
struct StoryResult {
    objectID: Option<String>,
    title: Option<String>,
    url: Option<String>,
}

#[derive(Debug, Eq, Hash, Serialize)]
struct Story {
    objectID: String,
    title: String,
    url: String,
}

impl PartialEq for StoryResult {
    fn eq(&self, other: &Self) -> bool {
        (self.objectID == other.objectID) || (self.url == other.url) || (self.title == other.title)
    }
}

impl PartialEq for Story {
    fn eq(&self, other: &Self) -> bool {
        (self.objectID == other.objectID) || (self.url == other.url) || (self.title == other.title)
    }
}

fn main() {
    let today: DateTime<Utc> = Utc::now() - Duration::days(1);
    let previous_stories = get_previous_stories(today);
    let new_stories = find_new_stories(today, previous_stories);
    println!("{:#?}", new_stories);
    write_new_stories(today, new_stories);
}

fn get_previous_stories(today: DateTime<Utc>) -> Vec<Story> {
    let dates = get_dates(today);
    println!("{:#?}", dates);
    dates
        .into_iter()
        .filter_map(|date| get_previous_stories_from_day(date).ok())
        .flatten()
        .filter_map(convert_good)
        .collect()
}

fn convert_good(story_result: StoryResult) -> Option<Story> {
    match (story_result.objectID, story_result.title, story_result.url) {
        (Some(objectID), Some(title), Some(url)) => Some(Story {
            objectID,
            title,
            url,
        }),
        _ => None,
    }
}

fn get_dates(start_date: DateTime<Utc>) -> Vec<DateTime<Utc>> {
    (1..=N_DAYS)
        .map(|delta| start_date - Duration::days(delta))
        .collect()
}

fn get_previous_stories_from_day(
    date: DateTime<Utc>,
) -> Result<Vec<StoryResult>, Box<dyn std::error::Error>> {
    let request_url = format!("https://zhu.blue/hn/{}/data.json", date.format("%Y-%m-%d"));
    let stories = reqwest::blocking::get(request_url)?.json::<Vec<StoryResult>>()?;
    Ok(stories)
}

fn find_new_stories(start_date: DateTime<Utc>, previous_stories: Vec<Story>) -> Vec<Story> {
    let previous_stories: HashSet<Story, RandomState> =
        HashSet::from_iter(previous_stories.into_iter());
    let oldest_date = start_date - Duration::days(OLDEST_DATE_HN);
    (0..N_PAGES)
        .flat_map(|page| get_result_page(oldest_date, start_date, page))
        .filter_map(convert_good)
        .filter(|story| !previous_stories.contains(story))
        .enumerate()
        .take_while(|(i, _)| i < &N_DAILY_STORIES_OUTPUT)
        .map(|(_, story)| story)
        .collect()
}

fn get_result_page(
    oldest_date: DateTime<Utc>,
    current_date: DateTime<Utc>,
    page: u32,
) -> Vec<StoryResult> {
    let res = _get_result_page(oldest_date, current_date, page);
    match res {
        Ok(res) => match res.hits {
            Some(hits) => hits,
            _ => Vec::new(),
        },
        _ => Vec::new(),
    }
}

fn _get_result_page(
    oldest_date: DateTime<Utc>,
    current_date: DateTime<Utc>,
    page: u32,
) -> Result<RequestResult, Box<dyn std::error::Error>> {
    println!("Fetching page number {}.", page);
    let request_url = format!(
        "https://hn.algolia.com/api/v1/search?tags=(story,ask_hn,show_hn)&numericFilters=created_at_i>{},created_at_i<{}&page={}",
        oldest_date.timestamp(), current_date.timestamp(), page
    );
    println!("Request url: {}.", request_url);
    let result = reqwest::blocking::get(request_url)?.json::<RequestResult>()?;
    Ok(result)
}

fn write_new_stories(today: DateTime<Utc>, stories: Vec<Story>) {
    let today_str = today.format("%Y-%m-%d").to_string();
    match create_dir(format!("{}", &today_str)) {
        Ok(()) => println!("Created dir {}.", today_str),
        Err(_e) => println!("Couldn't create dir {}.", today_str),
    };
    write_json(&today_str, &stories).expect("Failed to write json file.");
    write_html(today, &stories).expect("Failed to write html file.");
}

fn write_json(today: &str, stories: &Vec<Story>) -> Result<(), Box<dyn std::error::Error>> {
    let json = serde_json::to_string_pretty(stories).unwrap();
    let mut file = File::create(format!("{today}/data.json"))?;
    file.write_all(json.as_ref())?;
    Ok(())
}

fn write_html(
    today: DateTime<Utc>,
    stories: &Vec<Story>,
) -> Result<(), Box<dyn std::error::Error>> {
    let stories = format_stories(today, stories);
    let mut file = File::create(format!("{}/index.html", today.format("%Y-%m-%d")))?;
    file.write_all(stories.as_ref())?;
    Ok(())
}

fn format_stories(today: DateTime<Utc>, stories: &Vec<Story>) -> String {
    let elements: Vec<String> = stories
        .into_iter()
        .map(|story| {
            let comment = format!("https://news.ycombinator.com/item?id={}", story.objectID);
            format!(
                "<dt><a href=\"{}\">{}</a></dt> <dd><a href=\"{}\">comments</a></dd>",
                story.url, story.title, comment
            )
        })
        .map(|element| format!("<div>{}</div>", element))
        .collect();
    let best_stories = format!("<dl>{}</dl>", elements.join("\n"));
    let date = today.format("%Y-%m-%d");
    let yesterday = (today - Duration::days(1)).format("%Y-%m-%d");
    let tomorrow = (today + Duration::days(1)).format("%Y-%m-%d");
    let yesterday = format!("<a href=\"../{}\">◀◀ yesterday</a>", yesterday);
    let tomorrow = format!("<a href=\"../{}\">tomorrow ▶▶</a>", tomorrow);
    let formatted = format!(
        "<!DOCTYPE html><html lang=\"en\">
        <head>
            <meta charset=\"UTF-8\">
            <title>Hacker News - {date}</title>
            <meta name=\"viewport\" content=\"width=device-width\" />
            <style>
                :root {{color-scheme: light dark;}}
                html {{margin: auto; max-width: 70ch;}}
                h1 {{text-align: center;}}
                a {{color: CanvasText; text-decoration: None;}}
                dl {{display: flex; flex-direction: column; gap: .7rem;}}
                dd a {{text-decoration: underline; text-decoration-style: dotted; color: ActiveText;}}
                nav ul {{list-style-type: none; display: flex; gap: 1rem; margin-top: 3rem;}}
            </style>
        </head>
        <body>
            <main>
            <h1>{date}</h1>
            {best_stories}
            </main>
        <nav><ul><li>{yesterday}</li><li>{tomorrow}</li></ul></nav>
        </body></html>"
    );
    println!("{}", formatted);
    formatted
}
